class Person:

    def __init__(self, surname, Firstname, nickname, birth_date):
        self.surname = surname
        self.Firstname = Firstname
        self.nickname = nickname
        self.birth_date = birth_date

    def get_age(self):
        return 2018 - self.birth_date

    def get_fullname(self):
        return self.Firstname + " " + self.surname


c = Person("Fliagin", "Vlad", "Vladosik",1920)

print (c.get_age())
print (c.get_fullname())
